<?php

declare(strict_types=1);

namespace Drupal\fontawesome_route_icons\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigImporterEvent;

/**
 * @todo Add description for this subscriber.
 */
final class ConfigImportSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a ConfigImportSubscriber object.
   */
  public function __construct(
    private readonly CacheTagsInvalidatorInterface $cacheTagsInvalidator,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ConfigEvents::IMPORT => 'onConfigImport'
    ];
  }

  /**
   * invalidate local_task cache tag if fontawesome_route_icons.settings config changes on import
   */
  public function onConfigImport(ConfigImporterEvent $event) {
    foreach(["create", "update", "delete"] as $op) {
      if (in_array("fontawesome_route_icons.settings", $event->getChangelist()[$op])) {
        $this->cacheTagsInvalidator->invalidateTags(['local_task']);
        return;
      }
    }
  }
}
