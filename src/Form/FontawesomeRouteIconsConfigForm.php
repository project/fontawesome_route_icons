<?php

namespace Drupal\fontawesome_route_icons\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

function flatten_array($array, $prefix = null) {
  if ($prefix) $prefix .= '.';

  $items = array();

  foreach ($array as $key => $value) {
    if (is_array($value))
      $items = array_merge($items,  flatten_array($value, $prefix . $key));
    else
      $items[$prefix . $key] = $value;
  }

  return $items;
}

/**
 * Defines a form that configures forms module settings.
 */
class FontawesomeRouteIconsConfigForm extends ConfigFormBase
{
  public const DOT_SUBSTITUTION = '$';

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'fontawesome_route_icons_configuration';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'fontawesome_route_icons.settings',
    ];
  }

  private function textarea_default()
  {
    $icons = \Drupal::config('fontawesome_route_icons.settings')->getRawData();

    $text = '';
    foreach($flattenend as $pattern => $icon) {
      $text .= $pattern . '|' . $icon . PHP_EOL;
    }

    return $text;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['patterns'] = [
      '#type' => 'textarea',
      '#title' => t('Route Icon Patterns'),
      '#default_value' => $this->textarea_default(),
      '#rows' => '10',
      '#description' => t('Enter one value per line, in the format pattern|fa-icon. Use * to target any route of specific pattern. example: entity.*.canonical|fa-eye'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config = $this->config('fontawesome_route_icons.settings');

    $lines = explode(PHP_EOL, $form_state->getValue('patterns'));
    foreach ($lines as $line) {
      $exploded = explode('|', $line);
      $config->set($exploded[0], $exploded[1]);
    }
    $config->save();
    \Drupal::service('router.builder')->rebuild();

    parent::submitForm($form, $form_state);
  }
}
